# Brutal Chess

## Description
A school project, BrutalChess is a chess game that aims to break up the pacing of the typical chess game with live battles, as well as provide a bit more accessibility to players who are newer to chess, or for any other reason need a competitive advantage over the computer player.

Battles are triggered every time a player (AI or human) tries to capture a piece. The stakes are: should the capturer win the battle, they can captue the piece and regular chess play continues; should the hopeful capturer lose the battle, they forfeit the right to move the piece with which they intended to capture (the piece is 'stunned'), and they must choose a different move, if indeed any moves are available to them. This significantly reduces the advantage a strong chess AI can have if the human player is very skilled at shooting games. The human player can brute-force their way against the machine!

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Build and Run
**Building and running from 0 to 100 real quick:**
(run this from within the repo)

```bash
cmake -S BrutalChess -B bin
cd bin
make
./BrutalChess
```

## Authors and acknowledgment
**All credit goes to the glourious Super Robot Dev Team Hyper Force Go!**
Plus some external libraries whose copyrights and licenses are included in the source.

## Project status
In development

